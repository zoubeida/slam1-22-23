# SLAM1-22-23
## Cours SQL 
Zoubeïda ABDELMOULA

## IMPORTANT : 
récupérer le lien onion sur https://www/~fred pour accéder à TJENER : copier le lien sur votre `README.md` de voter dépôt 

#### Mission 1 : Ecole.sql (Systèmatiquement exporter ecole.sql et déposer sur gitlab)

- [ ] Créer les tables `Eleve`, `Professeur`, `Classe` et `Enseignement` 
- [ ] Ajouter les clés primaires
- [ ] Ajouter les clés étrangères

- [ ] Remplir les tables avec des données cohérentes 
    - [ ] 10 élèves
    - [ ] 2 Professeurs
    - [ ] 2 classes (5 élèves par classe)
    - [ ] 4 enseignements (2 par classe par prof)

- [ ] prendre en note toutes les requêtes (textuelle) et leur code SQL et une capture de preuve
